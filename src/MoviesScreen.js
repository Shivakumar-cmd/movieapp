import {
  ActivityIndicator,
  FlatList,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import List from './component/list';
import _ from 'lodash';
const Movies = props => {
  const [trending, setTrending] = useState([]);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [offset, setOffset] = useState(1);
  const [search, setSearch] = useState('');
  const [input, setInput] = useState('');
  const [result, setResult] = useState([]);
  const [errorMsg, setErrorMsg] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const inputRef = useRef();

  useEffect(() => {
    // initialize debounce function to search once user has stopped typing every half second
    inputRef.current = _.debounce(onSearchText, 300);
  }, []);

  const onSearchText = async input => {
    setIsLoading(true);
    try {
      const response = await fetch(
        `https://api.themoviedb.org/3/search/movie?api_key=bb78e4cf3442e302d928f2c5edcdbee1&language=en-US&page=1&include_adult=false&query=${input}`,
      );
      let json = await response.json();

      let newArray = json;
      setOffset(newArray.page);
      setResult(newArray.results);
      setIsLoading(false);
    } catch (error) {}
  };

  const handleInputChange = event => {
    const input = event;
    setInput(input);
    inputRef.current(input);
  };
  const getTrendingMoviesFromApiAsync = async () => {
    try {
      const response = await fetch(
        'https://api.themoviedb.org/3/trending/all/day?api_key=bb78e4cf3442e302d928f2c5edcdbee1',
      );
      let json = await response.json();

      let newArray = json.results;

      setTrending(newArray);
      setIsRefreshing(false);
    } catch (error) {}
  };
  useEffect(() => {
    getTrendingMoviesFromApiAsync();
  }, []);
  const onRefresh = () => {
    //set isRefreshing to true
    setIsRefreshing(true);
    setInput('');
    getTrendingMoviesFromApiAsync();
    // and set isRefreshing to false at the end of your callApiMethod()
  };
  const renderTopRated = ({item}) => {
    // console.log(item);
    return (
      <List
        data={item}
        onPress={() => props.navigation.navigate('Detailed', {data: item})}
      />
    );
  };

  const loadMoreData = async () => {
    setIsLoading(true);
    let newoffset = offset + 1;
    try {
      const response = await fetch(
        `https://api.themoviedb.org/3/search/movie?api_key=bb78e4cf3442e302d928f2c5edcdbee1&language=en-US&page=${newoffset}&include_adult=false&query=${input}`,
      );

      let json = await response.json();

      let newArray = json;
      let results = newArray.results;

      setOffset(newoffset);
      setResult(oldArray => [...oldArray, ...results]);
      setIsLoading(false);
    } catch (error) {}
  };

  function renderFooter() {
    return (
      //Footer View with Load More button
      <View
        style={{
          padding: 10,
          justifyContent: 'center',
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <TouchableOpacity
          activeOpacity={0.9}
          onPress={loadMoreData}
          //On Click of button calling loadMoreData function to load more data
          style={{
            padding: 10,
            // paddingBottom: 30,
            backgroundColor: '#800000',
            borderRadius: 4,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{color: 'white', fontSize: 15, textAlign: 'center'}}>
            Loading
          </Text>
          {isLoading ? (
            <ActivityIndicator color="white" style={{marginLeft: 8}} />
          ) : null}
        </TouchableOpacity>
      </View>
    );
  }
  return (
    <>
      <TextInput
        value={input}
        onChangeText={handleInputChange}
        style={{
          marginHorizontal: 16,
          borderWidth: 1,
          borderColor: 'grey',
          marginTop: 10,
        }}
      />
      <View style={{flex: 1}}>
        {input.length !== 0 ? (
          <FlatList
            data={result}
            renderItem={renderTopRated}
            contentContainerStyle={{marginTop: 12, paddingHorizontal: 6}}
            onEndReached={loadMoreData}
            onEndReachedThreshold={0.5}
            ListFooterComponent={renderFooter}
          />
        ) : (
          <FlatList
            data={trending}
            renderItem={renderTopRated}
            contentContainerStyle={{marginTop: 12, paddingHorizontal: 6}}
            onRefresh={onRefresh}
            refreshing={isRefreshing}
          />
        )}
      </View>
    </>
  );
};

export default Movies;

const styles = StyleSheet.create({});
