import {View, Text, Pressable, Image} from 'react-native';
import React from 'react';
import StarBorder from 'react-native-vector-icons/FontAwesome';
const List = ({data, onPress}) => {
  return (
    <Pressable onPress={onPress}>
      <View style={{padding: 10}}>
        <View style={{borderRadius: 8, flexDirection: 'row'}}>
          <Image
            source={{
              uri: `https://image.tmdb.org/t/p/w500/${data.backdrop_path}`,
            }}
            style={{
              width: 85,
              height: 128,
              borderRadius: 8,
            }}
          />

          <View
            style={{
              // flexDirection: 'row',
              // justifyContent: 'space-between',
              padding: 8,
              marginLeft: 8,
              flex: 1,
            }}>
            <Text
              ellipsizeMode="tail"
              numberOfLines={2}
              style={{color: 'black', fontWeight: 'bold', marginBottom: 8}}>
              {data.original_title}
            </Text>
            <Text
              ellipsizeMode="tail"
              numberOfLines={4}
              style={{
                color: 'black',
                fontWeight: '900',
                marginBottom: 8,
                flex: 1,
              }}>
              {data.overview}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                // justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <StarBorder name="star" size={12} color={'#FFC319'} />
              <Text style={{color: '#9C9C9C', marginLeft: 2}}>
                {data.vote_average}/10 IMDb
              </Text>
            </View>
          </View>
        </View>
      </View>
    </Pressable>
  );
};

export default List;
