import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {colors} from '../constants';
import Icon from 'react-native-vector-icons/MaterialIcons';
const Headinmg = ({left, onPress}) => {
  return (
    <View
      style={{
        marginHorizontal: 16,
        flexDirection: 'row',
        justifyContent: 'space-between',
      }}>
      <Text
        style={{
          fontSize: 18,
          fontWeight: 'bold',
          color: colors.primary,
        }}>
        {left}
      </Text>
      <TouchableOpacity onPress={onPress}>
        <View
          style={{
            flexDirection: 'row',
            borderColor: '#AAA9B1',
            borderWidth: 1,
            padding: 4,
            paddingHorizontal: 12,
            borderRadius: 20,
          }}>
          <Text
            style={{
              fontSize: 10,

              color: '#AAA9B1',
              // marginRight: 10,
            }}>
            See more
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default Headinmg;

const styles = StyleSheet.create({});
