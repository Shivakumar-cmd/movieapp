import React, {useRef, useState, useEffect} from 'react';
import Carousel, {Pagination, ParallaxImage} from 'react-native-snap-carousel';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Platform,
} from 'react-native';
const ENTRIES1 = [
  {
    title: 'Beautiful and dramatic Antelope Canyon',
    subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
    illustration: 'https://i.imgur.com/UYiroysl.jpg',
  },
  {
    title: 'Earlier this morning, NYC',
    subtitle: 'Lorem ipsum dolor sit amet',
    illustration: 'https://i.imgur.com/UPrs1EWl.jpg',
  },
  {
    title: 'White Pocket Sunset',
    subtitle: 'Lorem ipsum dolor sit amet et nuncat ',
    illustration: 'https://i.imgur.com/MABUbpDl.jpg',
  },
  {
    title: 'Acrocorinth, Greece',
    subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
    illustration: 'https://i.imgur.com/KZsmUi2l.jpg',
  },
  {
    title: 'The lone tree, majestic landscape of New Zealand',
    subtitle: 'Lorem ipsum dolor sit amet',
    illustration: 'https://i.imgur.com/2nCt3Sbl.jpg',
  },
];
const {width: screenWidth} = Dimensions.get('window');

const Scrollable = props => {
  const [entries, setEntries] = useState([]);
  const carouselRef = useRef(null);
  const [state, setState] = useState({
    activeSlide: 0,
  });
  const goForward = () => {
    carouselRef.current.snapToNext();
  };

  useEffect(() => {
    setEntries(props.data);
  }, [props]);

  const renderItem = ({item, index}, parallaxProps) => {
    return (
      <View style={styles.item} key={index}>
        <ParallaxImage
          // source={uri:+item.illustration}
          source={{
            uri: `https://image.tmdb.org/t/p/w500/${item.backdrop_path}`,
          }}
          // source={require(item.illustration)}
          containerStyle={styles.imageContainer}
          style={styles.image}
          parallaxFactor={0.0}
          {...parallaxProps}
        />
      </View>
    );
  };
  return (
    <View>
      <Carousel
        ref={carouselRef}
        sliderWidth={screenWidth}
        // sliderHeight={screenWidth}
        itemWidth={screenWidth - 60}
        data={entries}
        renderItem={renderItem}
        hasParallaxImages={true}
        onSnapToItem={index => setState({activeSlide: index})}
      />
      <Pagination
        dotsLength={entries.length}
        activeDotIndex={state.activeSlide}
        containerStyle={{
          position: 'absolute',
          // top: 0,
          left: 0,
          right: 0,
          bottom: -50,
          justifyContent: 'center',
          alignItems: 'center',
        }}
        dotStyle={{
          width: 21,
          height: 7,
          borderRadius: 5,
          backgroundColor: 'blue',
          // marginHorizontal: 8,
          // backgroundColor: "rgba(255, 255, 255, 0.92)",
        }}
        inactiveDotStyle={{
          // Define styles for inactive dots here
          width: 10,
          height: 10,
          borderRadius: 10 / 2,
          // marginHorizontal: 8,
          backgroundColor: '#33A6F3',
        }}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
      />
    </View>
  );
};

export default Scrollable;
const styles = StyleSheet.create({
  headeingcontainer: {},
  heading: {
    fontSize: 15,
    color: '#333333',

    marginHorizontal: 15,
    marginVertical: 10,
  },
  viewall: {
    fontSize: 15,
    color: '#00A7FF',

    marginVertical: 10,
    marginHorizontal: 15,
  },
  container: {
    flex: 1,
  },
  item: {
    width: screenWidth - 800,
    height: screenWidth / 2,
  },
  imageContainer: {
    flex: 1,
    marginBottom: Platform.select({ios: 0, android: 1}), // Prevent a random Android rendering issue

    borderRadius: 8,
  },
  image: {
    ...StyleSheet.absoluteFillObject,
    resizeMode: 'stretch',
  },
});
