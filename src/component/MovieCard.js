import {
  Image,
  Pressable,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableHighlightBase,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import Satr from 'react-native-vector-icons/Entypo';
import {colors} from '../constants';
const MovieCard = ({onPress, data}) => {
  return (
    <Pressable onPress={onPress}>
      <View
        style={{
          paddingHorizontal: 10,
          //   backgroundColor: 'red',
        }}>
        <View>
          <Image
            source={{
              uri: `https://image.tmdb.org/t/p/w500/${data.backdrop_path}`,
            }}
            style={{width: 143, height: 212, borderRadius: 8}}
            // resizeMode="cover"
          />
        </View>

        <View style={{width: 143, marginVertical: 10}}>
          <Text
            numberOfLines={2}
            ellipsizeMode="tail"
            style={{fontWeight: 'bold', color: 'black'}}>
            {data.original_title}
          </Text>
        </View>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <View style={{marginRight: 4}}>
            <Satr name="star" size={18} color={'#FFC319'} />
          </View>
          <View>
            <Text style={{color: '#9C9C9C'}}>{data.vote_average}/10 IMDb</Text>
          </View>
        </View>
      </View>
    </Pressable>
  );
};

export default MovieCard;

const styles = StyleSheet.create({});
