import {FlatList, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import List from './component/list';

const ListScreen = props => {
  console.log(props.route.params.data);
  const renderTopRated = ({item}) => {
    // console.log(item);
    return (
      <List
        data={item}
        onPress={() => props.navigation.navigate('Detailed', {data: item})}
      />
    );
  };
  return (
    <FlatList
      // horizontal={true}
      showsHorizontalScrollIndicator={false}
      data={props.route.params.data}
      renderItem={renderTopRated}
      contentContainerStyle={{marginTop: 12, paddingHorizontal: 6}}
    />
  );
};

export default ListScreen;

const styles = StyleSheet.create({});
