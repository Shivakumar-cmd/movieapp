/* eslint-disable react-native/no-inline-styles */
import {
  FlatList,
  Image,
  Pressable,
  StyleSheet,
  Text,
  useWindowDimensions,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {colors} from './constants';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Headinmg from './component/headinmg';
import MovieCard from './component/movieCard';
import Scrollable from './component/scrollable';

import List from './component/list';
const HomeScreen = props => {
  const {width: windowWidth} = useWindowDimensions();
  const [popular, setPopular] = useState([]);
  const [allPopular, setAllPopular] = useState([]);
  const [trending, setTrending] = useState([]);
  const [upcoming, setUpComing] = useState([]);
  const [topRated, setTopRated] = useState([]);
  const [allTopRated, setAllTopRated] = useState([]);
  const [threatres, setTheatres] = useState([]);
  useEffect(() => {
    const getMoviesFromApiAsync = async () => {
      try {
        const response = await fetch(
          'https://api.themoviedb.org/3/movie/popular?api_key=bb78e4cf3442e302d928f2c5edcdbee1&language=en-US&page=1',
        );
        let json = await response.json();

        let newArray = json.results.slice(0, 5);
        setPopular(newArray);
        setAllPopular(json.results);
      } catch (error) {}
    };
    const getTrendingMoviesFromApiAsync = async () => {
      try {
        const response = await fetch(
          'https://api.themoviedb.org/3/trending/all/day?api_key=bb78e4cf3442e302d928f2c5edcdbee1',
        );
        let json = await response.json();

        let newArray = json.results.slice(0, 5);
        setTrending(newArray);
      } catch (error) {}
    };
    const getUpcomingMoviesFromApiAsync = async () => {
      try {
        const response = await fetch(
          'https://api.themoviedb.org/3/movie/upcoming?api_key=bb78e4cf3442e302d928f2c5edcdbee1&language=en-US&page=1',
        );
        let json = await response.json();

        let newArray = json.results.slice(0, 5);
        setUpComing(newArray);
      } catch (error) {}
    };
    const getTopRatedMoviesFromApiAsync = async () => {
      try {
        const response = await fetch(
          'https://api.themoviedb.org/3/movie/top_rated?api_key=bb78e4cf3442e302d928f2c5edcdbee1&language=en-US&page=1',
        );
        let json = await response.json();

        let newArray = json.results.slice(0, 5);
        setTopRated(newArray);
        setAllTopRated(json.results);
      } catch (error) {}
    };
    const getTheatresMoviesFromApiAsync = async () => {
      try {
        const response = await fetch(
          'https://api.themoviedb.org/3/tv/on_the_air?api_key=bb78e4cf3442e302d928f2c5edcdbee1&language=en-US&page=1',
        );
        let json = await response.json();

        let newArray = json.results.slice(0, 5);
        setTheatres(newArray);
      } catch (error) {}
    };

    getMoviesFromApiAsync();
    getUpcomingMoviesFromApiAsync();
    getTopRatedMoviesFromApiAsync();
    getTrendingMoviesFromApiAsync();
    getTheatresMoviesFromApiAsync();
  }, []);

  const renderPopular = ({item, index}) => {
    return (
      <MovieCard
        onPress={() => props.navigation.navigate('Detailed', {data: item})}
        // onPress={() => console.log('dssd', item)}
        data={item}
      />
    );
  };
  const renderTrending = ({item, index}) => {
    return (
      <MovieCard
        onPress={() => props.navigation.navigate('Detailed', {data: item})}
        // onPress={() => console.log('dssd', item)}
        data={item}
      />
    );
  };
  const renderUpcoming = ({item, index}) => {
    return (
      <MovieCard
        onPress={() => props.navigation.navigate('Detailed', {data: item})}
        // onPress={() => console.log('dssd', item)}
        data={item}
      />
    );
  };

  const renderTopRated = ({item}) => {
    // console.log(item);
    return (
      <List
        data={item}
        onPress={() => props.navigation.navigate('Detailed', {data: item})}
      />
    );
  };
  const ListHeaderComponent = () => {
    return (
      <View>
        <View style={{marginTop: 10}}>
          <Headinmg
            left={'Now Showing'}
            onPress={() =>
              props.navigation.navigate('ListScreen', {data: allPopular})
            }
          />
          <FlatList
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            data={popular}
            renderItem={renderPopular}
            contentContainerStyle={{marginTop: 12, paddingHorizontal: 6}}
          />
        </View>
        {/* <View style={{marginTop: 24}}>
          <Headinmg left={'Playing In Theatres'} />

          <View style={{marginTop: 20}}>
            <Scrollable data={threatres} />
          </View>
        </View> */}
        {/* <View style={{marginTop: 34}}>
          <Headinmg left={'Trending'} />
          <FlatList
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            data={trending}
            renderItem={renderTrending}
            contentContainerStyle={{marginTop: 12, paddingHorizontal: 6}}
          />
        </View> */}
        <View style={{marginTop: 24}}>
          <Headinmg
            left={'Popular'}
            onPress={() => {
              props.navigation.navigate('ListScreen', {data: allTopRated});
            }}
          />
          <FlatList
            // horizontal={true}
            showsHorizontalScrollIndicator={false}
            data={topRated}
            renderItem={renderTopRated}
            contentContainerStyle={{marginTop: 12, paddingHorizontal: 6}}
          />
        </View>
        {/* <View style={{marginTop: 24}}>
          <Headinmg left={'Upcoming'} />
          <FlatList
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            data={upcoming}
            renderItem={renderUpcoming}
            contentContainerStyle={{marginTop: 12, paddingHorizontal: 6}}
          />
        </View> */}
      </View>
    );
  };
  return (
    <View style={{flex: 1, backgroundColor: colors.white}}>
      <FlatList ListHeaderComponent={ListHeaderComponent} />
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({});
