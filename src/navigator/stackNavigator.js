import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import HomeScreen from '../homeScreen';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Bell from 'react-native-vector-icons/FontAwesome';
import Menu from '../assets/Menu.svg';

import {colors} from '../constants';
export const HomestackNavigator = () => {
  const Stack = createNativeStackNavigator();
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{
          headerLeft: props => (
            // <Icon
            //   name="menu"
            //   size={props.size ? props.size : 24}
            //   color={colors.primary}
            // />
            <Menu width={20} height={20} fill={colors.primary} />
          ),
          headerRight: props => (
            <>
              <Bell
                name="bell-o"
                size={props.size ? props.size : 20}
                color={colors.primary}
              />
              <View
                style={{
                  height: 7,
                  width: 7,
                  borderRadius: 7 / 2,
                  backgroundColor: 'red',
                  position: 'absolute',
                  top: 2,
                  left: 10,
                }}
              />
            </>
          ),
          headerTitle: () => (
            <Text
              style={{
                fontSize: 18,
                paddingLeft: 10,
                fontWeight: 'bold',
                color: colors.primary,
              }}>
              Film Ku
            </Text>
          ),
          headerTitleAlign: 'center',
          headerShadowVisible: false,
        }}
      />
    </Stack.Navigator>
  );
};

const styles = StyleSheet.create({});
