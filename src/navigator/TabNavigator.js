import {StyleSheet, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Movie from '../assets/Bookmark.svg';
import Icon2 from '../assets/Bookmark Copy.svg';
import Icon3 from '../assets/Bookmark Copy 2.svg';
import React from 'react';
import {
  AnimatedTabBarNavigator,
  DotSize,
  TabElementDisplayOptions,
} from 'react-native-animated-nav-tab-bar';
import {HomestackNavigator} from './stackNavigator';
import Movies from '../moviesScreen';
import UserScreen from '../userScreen';
import {colors} from '../constants';

const TabNavigator = () => {
  const Tabs = AnimatedTabBarNavigator();
  const TabBarIcon = props => {
    return (
      <Icon
        name={props.name}
        size={props.size ? props.size : 24}
        color={props.tintColor}
      />
    );
  };
  return (
    <Tabs.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        activeTintColor: '#57ABFA',
        // inactiveTintColor: '#8CBAD8',
        activeBackgroundColor: 'white',
      }}
      appearance={
        {
          // shadow: true,
          // floating: true,
          // whenActiveShow: TabElementDisplayOptions.ICON_ONLY,
          // dotSize: DotSize.SMALL,
        }
      }>
      <Tabs.Screen
        name="Movies"
        component={HomestackNavigator}
        options={{
          tabBarIcon: ({focused, color}) => (
            // <TabBarIcon
            //   // focused={focused}
            //   tintColor={color}
            //   name="local-movies"
            // />
            <Movie
              width={24}
              height={24}
              fill={focused ? colors.primary : '#D8D8D8'}
            />
          ),
          tabBarLabel: ({}) => {
            <Text />;
          },
        }}
      />
      <Tabs.Screen
        name="Videos"
        component={Movies}
        options={{
          tabBarIcon: ({focused, color}) => (
            // <TabBarIcon
            //   focused={focused}
            //   tintColor={color}
            //   name="video-library"
            // />
            <Icon2
              width={24}
              height={24}
              fill={focused ? colors.primary : '#D8D8D8'}
            />
          ),
          tabBarLabel: ({}) => {
            <Text />;
          },
        }}
      />
      <Tabs.Screen
        name="User"
        component={UserScreen}
        options={{
          tabBarIcon: ({focused, color}) => (
            // <TabBarIcon
            //   focused={focused}
            //   tintColor={color}
            //   name="supervised-user-circle"
            // />
            <Icon3
              width={24}
              height={24}
              fill={focused ? colors.primary : '#D8D8D8'}
            />
          ),
          tabBarLabel: ({}) => {
            <Text />;
          },
        }}
      />
    </Tabs.Navigator>
  );
};

export default TabNavigator;

const styles = StyleSheet.create({});
