import {
  FlatList,
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {colors} from './constants';
import Back from 'react-native-vector-icons/MaterialIcons';
import Dots from 'react-native-vector-icons/MaterialCommunityIcons';
import StarBorder from 'react-native-vector-icons/FontAwesome';
import Headinmg from './component/headinmg';
import Scrollable from './component/scrollable';
import MovieCard from './component/movieCard';

const DetailedScreen = props => {
  const [simiklar, simmilarMoviews] = useState([]);
  const [getCarousal, setGetCarousal] = useState([]);

  useEffect(() => {
    const getTheatresMoviesFromApiAsync = async () => {
      try {
        // https://api.themoviedb.org/3/movie/438148/similar?api_key=bb78e4cf3442e302d928f2c5edcdbee1&language=en-US&page=1
        const response = await fetch(
          `https://api.themoviedb.org/3/movie/${props.route.params.data.id}/similar?api_key=bb78e4cf3442e302d928f2c5edcdbee1&language=en-US&page=1`,
        );
        let json = await response.json();

        let newArray = json.results.slice(0, 5);
        simmilarMoviews(newArray);
      } catch (error) {}
    };
    const getcarousal = async () => {
      try {
        const response = await fetch(
          'https://api.themoviedb.org/3/tv/on_the_air?api_key=bb78e4cf3442e302d928f2c5edcdbee1&language=en-US&page=1',
        );
        let json = await response.json();

        let newArray = json.results.slice(0, 5);

        setGetCarousal(newArray);
      } catch (error) {}
    };
    getTheatresMoviesFromApiAsync();
    getcarousal();
  }, [props]);

  const renderPopular = ({item}) => {
    return (
      <MovieCard
        onPress={() => props.navigation.navigate('Detailed')}
        data={item}
      />
    );
  };
  return (
    <View style={{flex: 1}}>
      <View>
        <ImageBackground
          source={{
            uri: `https://image.tmdb.org/t/p/w500/${props.route.params.data.backdrop_path}`,
          }}
          style={{height: 300}}>
          <View
            style={{
              margin: 16,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <Back
              name="keyboard-backspace"
              size={24}
              color={'white'}
              onPress={() => {
                props.navigation.goBack();
              }}
            />
            <Dots name="dots-horizontal" size={24} color={'white'} />
          </View>
        </ImageBackground>
      </View>
      {/* <View
          style={{
            paddingHorizontal: 16,

            paddingBottom: 20,
            marginTop: 20,
          }}>
          <Text>Videos</Text>
          <View style={{marginHorizontal: -20}}>
            <Scrollable data={getCarousal} />
          </View>
          <View style={{marginTop: 40}}>
            <Headinmg left={'Recomendd'} />
          </View>
          <FlatList
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            data={simiklar}
            renderItem={renderPopular}
            contentContainerStyle={{marginTop: 12, paddingHorizontal: 6}}
          />
        </View> */}
      <ScrollView>
        <View
          style={{
            // position: 'absolute',

            // marginTop: -20,
            margin: 16,
          }}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={{color: 'black', fontWeight: 'bold', marginBottom: 8}}>
              {props.route.params.data.original_title}
            </Text>
            <Dots name="bookmark-outline" size={24} color={'black'} />
          </View>
          <View
            style={{
              flexDirection: 'row',
              // justifyContent: 'space-between',
              alignItems: 'center',
              marginVertical: 8,
            }}>
            <StarBorder name="star" size={12} color={'#FFC319'} />
            <Text style={{color: '#9C9C9C', marginLeft: 2}}>
              {props.route.params.data.vote_average}/10 IMDb
            </Text>
          </View>
          <View
            style={{
              // marginHorizontal: 16,
              marginVertical: 10,
            }}>
            <Text
              style={{
                fontSize: 18,
                fontWeight: 'bold',
                color: colors.primary,
              }}>
              Description
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              // marginTop: 20,
            }}>
            <Text style={{color: '#9C9C9C', fontWeight: '600'}}>
              {props.route.params.data.overview}
            </Text>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default DetailedScreen;

const styles = StyleSheet.create({});
