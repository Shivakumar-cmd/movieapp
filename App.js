/* eslint-disable react-native/no-inline-styles */
import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {
  AnimatedTabBarNavigator,
  DotSize,
  TabElementDisplayOptions,
} from 'react-native-animated-nav-tab-bar';
import {NavigationContainer} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Bookamerk from 'react-native-vector-icons/Feather';
import HomeScreen from './src/homeScreen';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import Movies from './src/moviesScreen';
import UserScreen from './src/userScreen';
import {HomestackNavigator} from './src/navigator/stackNavigator';
import {colors} from './src/constants';
import TabNavigator from './src/navigator/tabNavigator';
import DetailedScreen from './src/detailedScreen';
import ListScreen from './src/listScreen';
const App = () => {
  const Stack = createNativeStackNavigator();

  const TabBarIcon = props => {
    return (
      <Icon
        name={props.name}
        size={props.size ? props.size : 24}
        color={props.tintColor}
      />
    );
  };
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Tab"
          component={TabNavigator}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Detailed"
          component={DetailedScreen}
          options={{
            headerRight: props => (
              <View
                style={{
                  flexDirection: 'row',

                  width: 80,
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <Icon
                  name="star-border"
                  size={props.size ? props.size : 24}
                  color={colors.primary}
                />
                <Bookamerk
                  name="heart"
                  size={props.size ? props.size : 24}
                  color={colors.primary}
                />
                <Bookamerk
                  name="bookmark"
                  size={props.size ? props.size : 24}
                  color={colors.primary}
                />
              </View>
            ),
            headerTitle: () => (
              <Text
                style={{
                  fontSize: 18,
                  paddingLeft: 10,
                  fontWeight: 'bold',
                  color: colors.primary,
                }}
              />
            ),
            headerShadowVisible: false,
            headerShown: false,
            contentStyle: {backgroundColor: 'white'},
          }}
        />
        <Stack.Screen
          name="ListScreen"
          component={ListScreen}
          options={{
            // headerRight: props => (
            //   <View
            //     style={{
            //       flexDirection: 'row',

            //       width: 80,
            //       justifyContent: 'space-between',
            //       alignItems: 'center',
            //     }}>
            //     <Icon
            //       name="star-border"
            //       size={props.size ? props.size : 24}
            //       color={colors.primary}
            //     />
            //     <Bookamerk
            //       name="heart"
            //       size={props.size ? props.size : 24}
            //       color={colors.primary}
            //     />
            //     <Bookamerk
            //       name="bookmark"
            //       size={props.size ? props.size : 24}
            //       color={colors.primary}
            //     />
            //   </View>
            // ),
            headerTitle: () => (
              <Text
                style={{
                  fontSize: 18,
                  paddingLeft: 10,
                  fontWeight: 'bold',
                  color: colors.primary,
                }}
              />
            ),
            headerShadowVisible: true,
            headerShown: true,
            contentStyle: {backgroundColor: 'white'},
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;

const styles = StyleSheet.create({});
